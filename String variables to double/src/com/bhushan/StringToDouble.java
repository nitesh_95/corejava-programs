package com.bhushan;

import java.util.Scanner;

public class StringToDouble {

	public static void main(String[] args) {
		String str;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		str = s.nextLine();

		double value = Double.parseDouble(str);
		System.out.println(value);
	}

}
