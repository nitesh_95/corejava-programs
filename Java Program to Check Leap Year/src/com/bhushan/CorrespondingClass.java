package com.bhushan;

import java.util.Scanner;

public class CorrespondingClass {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Year");
		int nextInt = s.nextInt();
		// year is divisible by 400, hence the year is a leap year
		if(((nextInt % 4 == 0) && (nextInt % 100!= 0)) || (nextInt % 400 == 0)){
			System.out.println("The year is leap Year");

		} else {
			System.out.println("This is not leap year");
		}

	}
}
