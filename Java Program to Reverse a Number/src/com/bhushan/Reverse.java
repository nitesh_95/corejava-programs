package com.bhushan;

import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {

		int num;
		int reverse = 0;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Digits");
		num = s.nextInt();

        while(num != 0) {
            int digit = num % 10;
            reverse = reverse * 10 + digit;
            num /= 10;
 
	}
        System.out.println(reverse);

}
}