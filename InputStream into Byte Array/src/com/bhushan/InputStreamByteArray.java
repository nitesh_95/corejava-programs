package com.bhushan;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
public class InputStreamByteArray {


		public static byte[] toByteArray(InputStream in) throws IOException {

			ByteArrayOutputStream os = new ByteArrayOutputStream();

			byte[] buffer = new byte[1024];
			int len;

			while ((len = in.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}

			return os.toByteArray();
		}

		public static void main(String[] args) throws IOException
		{
			InputStream in = new ByteArrayInputStream("Techie Delight"
											.getBytes(StandardCharsets.UTF_8));

			byte[] bytes = toByteArray(in);
			System.out.println(new String(bytes));
		}
	}