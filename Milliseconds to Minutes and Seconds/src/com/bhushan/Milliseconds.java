package com.bhushan;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Milliseconds {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter MilliSeconds");
		long milli = s.nextLong();
		long milliseconds = milli;

		long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);

		long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);

		System.out.format("%d Milliseconds = %d minutes\n", milliseconds, minutes);
		System.out.println("Or");
		System.out.format("%d Milliseconds = %d seconds", milliseconds, seconds);

	}
}