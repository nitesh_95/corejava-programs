package com.bhushan;

import java.util.Scanner;

public class IntToString {

	public static void main(String [] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Integer");
		int num = s.nextInt();
		
		String str = Integer.toString(num);
		System.out.println(str);
		
	}
}
