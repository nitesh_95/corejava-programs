package com.bhushan;

import java.util.Scanner;

public class OctalToDecimal {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Octal String");
		String str = s.nextLine();
		String octalString = str;
		int decimal = Integer.parseInt(octalString, 8);
		System.out.println(decimal);
	}
}
