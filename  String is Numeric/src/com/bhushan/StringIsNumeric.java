package com.bhushan;

import java.util.Scanner;

public class StringIsNumeric {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		String string = scanner.nextLine();
		boolean numeric = true;

		try {
			Double num = Double.parseDouble(string);
		} catch (NumberFormatException e) {
			numeric = false;
		}

		if (numeric)
			System.out.println(string + " is a number");
		else
			System.out.println(string + " is not a number");
	}
}
