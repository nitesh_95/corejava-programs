package com.bhushan;

import java.util.Scanner;

public class GCD {

	public static void main(String[] args) {
		int gcd = 1;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter First No");
		int n1 = s.nextInt();
		System.out.println("Enter Second No");
		int n2 = s.nextInt();

		  for(int i = 1; i <= n1 && i <= n2; ++i)
	        {
	            if(n1 % i==0 && n2 % i==0)
	                gcd = i;
	        }

	        System.out.printf("G.C.D of %d and %d is %d", n1, n2, gcd);
	    }
	}