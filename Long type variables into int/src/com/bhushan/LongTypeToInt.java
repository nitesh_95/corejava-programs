package com.bhushan;

import java.util.Scanner;

public class LongTypeToInt {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Data");
		long data = s.nextLong();
		int result = (int) data;
		System.out.println(result);
	}

}