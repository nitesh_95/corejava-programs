package com.bhushan;

import java.util.Scanner;

public class RemoveStringWhiteSpace {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = s.nextLine();

		String strs = str.replaceAll("\\s", "");
		System.out.println(strs);
	}
}
