package com.bhushan;

import java.util.Scanner;

public class calculator {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter First Number");
		double firstNumber = s.nextDouble();
		System.out.println("Enter Second Number");
		double secondNumber = s.nextDouble();
		
		System.out.println("Enter the operator");
		char operator = s.next().charAt(0);
		
		double result;
		
		switch (operator) {
		 case '+':
             result = firstNumber + secondNumber;
             break;

         case '-':
             result = firstNumber - secondNumber;
             break;

         case '*':
             result = firstNumber * secondNumber;
             break;

         case '/':
             result = firstNumber / secondNumber;
             break;
         default:
             System.out.printf("Error! operator is not correct");
             return;
     }

     System.out.println(result);
 }
}