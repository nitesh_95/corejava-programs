package com.bhushan;
import java.util.Scanner;

public class BinaryToOctal {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Binary");
		String str = s.nextLine();
		String binaryString = str;
		
		int i = Integer.parseInt(binaryString);
		String octal = Integer.toOctalString(i);
		System.out.println(octal);
	}
}
