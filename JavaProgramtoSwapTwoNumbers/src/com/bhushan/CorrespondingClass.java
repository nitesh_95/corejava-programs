package com.bhushan;

import java.util.Scanner;

public class CorrespondingClass {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter first no ");
		int a = s.nextInt();
		System.out.println("Enter Second no ");
		int b = s.nextInt();

		int swap = a;
		a = b;
		b = swap;

		System.out.println("Numbers after Swapping" + a + "...." + b);

	}

}
