package com.bhushan;

public class StringBuffers {

	public static void main(String args[]) {

		StringBuffer sb = new StringBuffer();
		sb.append("Hello ");
		sb.append("World ");
		sb.append("StringBuffer ");

		sb.delete(0, sb.length());

		sb.append("String deleted ");

		System.out.println(sb.toString());

	}
}
