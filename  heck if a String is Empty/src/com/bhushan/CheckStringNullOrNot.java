package com.bhushan;

import java.util.Scanner;

public class CheckStringNullOrNot {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = s.nextLine();

		if (str.length() == 0) {
			System.out.println("String is Empty");
		} else {
			System.out.println("String contains Data");
		}
	}

}
