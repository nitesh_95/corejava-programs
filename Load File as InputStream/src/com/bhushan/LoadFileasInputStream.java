package com.bhushan;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class LoadFileasInputStream {

	public static void main(String[] args) {
		InputStream is;
		try {
			is = new FileInputStream("E:\\CoreJava Programs\\Delete File in Java\\src\\com\\bhushan\\DeleteFile.java");

			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Converted to ByteStream");
	}
}
