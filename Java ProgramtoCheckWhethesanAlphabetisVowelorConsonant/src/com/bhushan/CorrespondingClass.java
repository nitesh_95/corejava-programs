package com.bhushan;

import java.util.Scanner;

public class CorrespondingClass {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter alphabet");
		char alphabet = s.next().charAt(0);
		if (alphabet == 'a' || alphabet == 'e' || alphabet == 'i' || alphabet == 'o' || alphabet == 'u') {
			System.out.println("Entered Charcter is Vowel");
		} else {
			System.out.println("Entered Character is Consonant");
		}

	}

}
