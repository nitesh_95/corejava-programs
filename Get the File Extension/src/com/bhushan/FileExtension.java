package com.bhushan;

import java.io.File;

public class FileExtension {
	private static String fileExtension(File file) {
		String name = file.getName();
		if (name.lastIndexOf(".") != -1 && name.lastIndexOf(".") != 0)
			return name.substring(name.lastIndexOf(".") + 1);
		else
			return "";
	}

	public static void main(String[] args) {
		File file = new File("demo1.txt");
		System.out.println("The file extension is: " + fileExtension(file));
	}
}