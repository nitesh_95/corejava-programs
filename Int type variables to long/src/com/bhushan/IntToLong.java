package com.bhushan;

import java.util.Scanner;

public class IntToLong {
	public static void main(String[]args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Integer");
		Integer num = s.nextInt();
		
		Long value = Long.valueOf(num);
		System.out.println(value);
		
	}

}
