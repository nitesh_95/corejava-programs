package com.bhushan;

import java.util.Scanner;

public class SumOfNaturalNo {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Decimal String");
		Integer num = s.nextInt();
		int number = num;
		int sum = addNumbers(number);
		System.out.println("Sum = " + sum);
	}

	public static int addNumbers(int num) {
		if (num != 0)
			return num + addNumbers(num - 1);
		else
			return num;
	}
}
