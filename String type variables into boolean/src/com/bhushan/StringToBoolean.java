package com.bhushan;

import java.util.Scanner;

public class StringToBoolean {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = s.nextLine();
		
		boolean value = Boolean.getBoolean(str);
		System.out.println(value);
	}
}
