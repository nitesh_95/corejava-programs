package com.bhushan;

public class ExecutionTimeMethods {

	public void display() {
		System.out.println("Calculating Method execution time:");
	}

	public static void main(String[] args) {

		ExecutionTimeMethods obj = new ExecutionTimeMethods();

		long start = System.nanoTime();

		obj.display();

		long end = System.nanoTime();

		long execution = end - start;
		System.out.println("Execution time: " + execution + " nanoseconds");
	}
}