package com.bhushan;

import java.util.Scanner;

public class IterateCharacter {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String");

		{
			String s = sc.nextLine();

			for (int i = 0; i < s.length(); i++) {
				System.out.print(s.charAt(i));
			}
		}
	}
}
