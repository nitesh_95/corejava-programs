package com.bhushan;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class CountLines {

	public static void main(String[]args) {
		 LineNumberReader reader = null;
		    try {
		      reader = new LineNumberReader(new FileReader(new File("E:\\CoreJava Programs\\Calculate Average Using Arrays\\src\\com\\bhushan\\Test.java")));
		      // Read file till the end
		      while ((reader.readLine()) != null);
		      System.out.println("Count of lines - " + reader.getLineNumber());
		    } catch (Exception ex) {
		      ex.printStackTrace();
		    } finally { 
		      if(reader != null){
		        try {
		          reader.close();
		        } catch (IOException e) {
		          // TODO Auto-generated catch block
		          e.printStackTrace();
		        }
		      }
		    }
		  }
		}