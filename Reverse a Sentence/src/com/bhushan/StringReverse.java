package com.bhushan;

import java.util.Scanner;

public class StringReverse {

	public static void main(String[] args) {
		String reverse = "";
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = s.nextLine();
		int length = str.length();
		for (int i = length - 1; i >= 0; i--) {
			reverse = reverse + str.charAt(i);
			
		}
		System.out.println(reverse);

	}
}
