package com.bhushan;

import java.util.Scanner;

public class StringToInt {
	public static void main(String[]args)
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = s.nextLine();
		
		Integer value = Integer.parseInt(str);
		System.out.println(value);
	}
}
