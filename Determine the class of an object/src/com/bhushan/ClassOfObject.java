package com.bhushan;

class Test1 {
	public String test() {
		return "welcome";
	}
}

class Test2 {
	public String test() {
		return "welcome";
	}
}

public class ClassOfObject {
	public static void main(String[] args) {
		Test1 test1 = new Test1();
		Test2 test2 = new Test2();

		System.out.println(test1.getClass());
		System.out.println(test1.test());
		System.out.println(test2.getClass());
		System.out.println(test2.test());
	}

}
