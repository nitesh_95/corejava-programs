package com.bhushan;

import java.util.Scanner;

public class Power {

    public static void main(String[] args) {
    	Scanner s = new Scanner(System.in);
    	System.out.println("Enter Base");
    	System.out.println("EnterPowerRaised");
        int base = s.nextInt(), powerRaised = s.nextInt();
        int result = power(base, powerRaised);

        System.out.printf("%d^%d = %d", base, powerRaised, result);
    }

    public static int power(int base, int powerRaised) {
        if (powerRaised != 0)
            return (base * power(base, powerRaised - 1));
        else
            return 1;
    }
}