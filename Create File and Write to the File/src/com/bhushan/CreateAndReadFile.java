package com.bhushan;

import java.io.File;

public class CreateAndReadFile {

	public static void main(String[]args) {
		boolean result;
		
		File file = new File("E:\\CoreJava Programs\\bhushan");
		try {
			result = file.createNewFile();
			
			if(result) {
				System.out.println("Created new file"+file.getCanonicalPath());
			}else {
				System.out.println("Already Exists file"+file.getCanonicalPath());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
