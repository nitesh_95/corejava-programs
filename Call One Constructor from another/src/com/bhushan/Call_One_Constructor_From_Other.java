package com.bhushan;

public class Call_One_Constructor_From_Other {

	private String name;
	private double salary;
	private  String address;
	
	 public Call_One_Constructor_From_Other()
	    {
	        this("Chaitanya");
	    }

	    public Call_One_Constructor_From_Other(String name)
	    {
	    	this(name, 120035);
	    }
	    public Call_One_Constructor_From_Other(String name, int sal)
	    {
	    	this(name, sal, "Gurgaon");
	    }
	    public Call_One_Constructor_From_Other(String name, int sal, String addr)
	    {
	    	this.name=name;
	    	this.salary=sal;
	    	this.address=addr;
	    }

	    void disp() {
	    	System.out.println("Employee Name: "+name);
	    	System.out.println("Employee Salary: "+salary);
	    	System.out.println("Employee Address: "+address);
	    }
	    public static void main(String[] args)
	    {
	    	Call_One_Constructor_From_Other obj = new Call_One_Constructor_From_Other();
	        obj.disp();
	    }
	}