package com.bhushan;

import java.util.Scanner;

public class BinaryToDecimal {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Binary");
		String str = s.nextLine();
		String binaryString = str;
		int decimal = Integer.parseInt(binaryString, 2);
		System.out.println(decimal);
	}
}
