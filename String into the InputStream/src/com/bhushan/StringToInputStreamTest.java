package com.bhushan;

import java.io.*;
public class StringToInputStreamTest {
   public static void main(String []args) throws Exception {
      String str = "Welcome to BhushanITSolutions";
      InputStream input = getInputStream(str, "UTF-8");
      int i;
      while ((i = input.read()) > -1) {
         System.out.print((char) i);
      }
      System.out.println();
   }
   public static InputStream getInputStream(String str, String encoding) throws UnsupportedEncodingException {
      return new ByteArrayInputStream(str.getBytes(encoding));
   }
}