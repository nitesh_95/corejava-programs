package com.bhushan;

import java.util.Scanner;

public class PrimeNo {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Enter Prime No Range Starting");
		int rangeStart = s.nextInt();
		System.out.println("Enter Prime No Range Ending");
		int rangeEnd =s.nextInt();
		
		 while (rangeStart < rangeEnd) {
	            boolean flag = false;

	            for(int i = 2; i <= rangeStart/2; ++i) {
	                // condition for nonprime number
	                if(rangeStart % i == 0) {
	                    flag = true;
	                    break;
	                }
	            }

	            if (!flag && rangeStart != 0 && rangeStart != 1)
	                System.out.print(rangeStart + " ");

	            ++rangeStart;
	        }
	    }
	}