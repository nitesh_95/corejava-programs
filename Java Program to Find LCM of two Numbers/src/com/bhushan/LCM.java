package com.bhushan;

import java.util.Scanner;

public class LCM {

	public static void main(String[] args) {

		int gcd =1 ;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter First Digit");
		int n1 = s.nextInt();
		System.out.println("Enter Second Digit");
		int n2 = s.nextInt();
		for(int i = 1; i <= n1 && i <= n2; ++i)
        {
            if(n1 % i == 0 && n2 % i == 0)
                gcd = i;
        }

        int lcm = (n1 * n2) / gcd;
        System.out.printf("The LCM of %d and %d is %d.", n1, n2, lcm);
    }
}
