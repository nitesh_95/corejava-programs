package com.bhushan;

import java.util.Scanner;

public class CharacterToString {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Character");
		char ch = s.next().charAt(0);

		String str = Character.toString(ch);
		System.out.println(str);
	}

}
