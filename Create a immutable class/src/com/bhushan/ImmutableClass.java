package com.bhushan;

public class ImmutableClass {

	public static void main(String[] args) {

		Student student = new Student("Nitesh", "BHushan");
		System.out.println(student.getName());
		System.out.println(student.getAddress());
		student.toString();
	}

}
